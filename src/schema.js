"use strict";
exports.__esModule = true;
exports.schema = void 0;
var prosemirror_model_1 = require("prosemirror-model");
/// Document schema for the data model used by CommonMark.
exports.schema = new prosemirror_model_1.Schema({
    nodes: {
        doc: {
            content: "block+"
        },
        paragraph: {
            content: "inline*",
            group: "block",
            parseDOM: [{ tag: "p" }],
            toDOM: function () { return ["p", 0]; }
        },
        blockquote: {
            content: "block+",
            group: "block",
            parseDOM: [{ tag: "blockquote" }],
            toDOM: function () { return ["blockquote", 0]; }
        },
        horizontal_rule: {
            group: "block",
            parseDOM: [{ tag: "hr" }],
            toDOM: function () { return ["div", ["hr"]]; }
        },
        heading: {
            attrs: { level: { "default": 1 } },
            content: "(text | image)*",
            group: "block",
            defining: true,
            parseDOM: [{ tag: "h1", attrs: { level: 1 } },
                { tag: "h2", attrs: { level: 2 } },
                { tag: "h3", attrs: { level: 3 } },
                { tag: "h4", attrs: { level: 4 } },
                { tag: "h5", attrs: { level: 5 } },
                { tag: "h6", attrs: { level: 6 } }],
            toDOM: function (node) { return ["h" + node.attrs.level, 0]; }
        },
        code_block: {
            content: "text*",
            group: "block",
            code: true,
            defining: true,
            marks: "",
            attrs: { params: { "default": "" } },
            parseDOM: [{ tag: "pre", preserveWhitespace: "full", getAttrs: function (node) { return ({ params: node.getAttribute("data-params") || "" }); } }],
            toDOM: function (node) { return ["pre", node.attrs.params ? { "data-params": node.attrs.params } : {}, ["code", 0]]; }
        },
        ordered_list: {
            content: "list_item+",
            group: "block",
            attrs: { order: { "default": 1 }, tight: { "default": false } },
            parseDOM: [{ tag: "ol", getAttrs: function (dom) {
                        return { order: dom.hasAttribute("start") ? +dom.getAttribute("start") : 1,
                            tight: dom.hasAttribute("data-tight") };
                    } }],
            toDOM: function (node) {
                return ["ol", { start: node.attrs.order == 1 ? null : node.attrs.order,
                        "data-tight": node.attrs.tight ? "true" : null }, 0];
            }
        },
        bullet_list: {
            content: "list_item+",
            group: "block",
            attrs: { tight: { "default": false } },
            parseDOM: [{ tag: "ul", getAttrs: function (dom) { return ({ tight: dom.hasAttribute("data-tight") }); } }],
            toDOM: function (node) { return ["ul", { "data-tight": node.attrs.tight ? "true" : null }, 0]; }
        },
        list_item: {
            content: "block+",
            defining: true,
            parseDOM: [{ tag: "li" }],
            toDOM: function () { return ["li", 0]; }
        },
        text: {
            group: "inline"
        },
        image: {
            inline: true,
            attrs: {
                src: {},
                alt: { "default": null },
                title: { "default": null }
            },
            group: "inline",
            draggable: true,
            parseDOM: [{ tag: "img[src]", getAttrs: function (dom) {
                        return {
                            src: dom.getAttribute("src"),
                            title: dom.getAttribute("title"),
                            alt: dom.getAttribute("alt")
                        };
                    } }],
            toDOM: function (node) { return ["img", node.attrs]; }
        },
        hard_break: {
            inline: true,
            group: "inline",
            selectable: false,
            parseDOM: [{ tag: "br" }],
            toDOM: function () { return ["br"]; }
        }
    },
    marks: {
        em: {
            parseDOM: [
                { tag: "i" }, { tag: "em" },
                { style: "font-style=italic" },
                { style: "font-style=normal", clearMark: function (m) { return m.type.name == "em"; } }
            ],
            toDOM: function () { return ["em"]; }
        },
        strong: {
            parseDOM: [
                { tag: "strong" },
                { tag: "b", getAttrs: function (node) { return node.style.fontWeight != "normal" && null; } },
                { style: "font-weight=400", clearMark: function (m) { return m.type.name == "strong"; } },
                { style: "font-weight", getAttrs: function (value) { return /^(bold(er)?|[5-9]\d{2,})$/.test(value) && null; } }
            ],
            toDOM: function () { return ["strong"]; }
        },
        link: {
            attrs: {
                href: {},
                title: { "default": null }
            },
            inclusive: false,
            parseDOM: [{ tag: "a[href]", getAttrs: function (dom) {
                        return { href: dom.getAttribute("href"), title: dom.getAttribute("title") };
                    } }],
            toDOM: function (node) { return ["a", node.attrs]; }
        },
        code: {
            parseDOM: [{ tag: "code" }],
            toDOM: function () { return ["code"]; }
        }
    }
});
