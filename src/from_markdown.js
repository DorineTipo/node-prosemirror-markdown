"use strict";
exports.__esModule = true;
exports.defaultMarkdownParser = exports.MarkdownParser = void 0;
// @ts-ignore
var markdown_it_1 = require("markdown-it");
var schema_1 = require("./schema");
var prosemirror_model_1 = require("prosemirror-model");
function maybeMerge(a, b) {
    if (a.isText && b.isText && prosemirror_model_1.Mark.sameSet(a.marks, b.marks))
        return a.withText(a.text + b.text);
}
// Object used to track the context of a running parse.
var MarkdownParseState = /** @class */ (function () {
    function MarkdownParseState(schema, tokenHandlers) {
        this.schema = schema;
        this.tokenHandlers = tokenHandlers;
        this.stack = [{ type: schema.topNodeType, attrs: null, content: [], marks: prosemirror_model_1.Mark.none }];
    }
    MarkdownParseState.prototype.top = function () {
        return this.stack[this.stack.length - 1];
    };
    MarkdownParseState.prototype.push = function (elt) {
        if (this.stack.length)
            this.top().content.push(elt);
    };
    // Adds the given text to the current position in the document,
    // using the current marks as styling.
    MarkdownParseState.prototype.addText = function (text) {
        if (!text)
            return;
        var top = this.top(), nodes = top.content, last = nodes[nodes.length - 1];
        var node = this.schema.text(text, top.marks), merged;
        if (last && (merged = maybeMerge(last, node)))
            nodes[nodes.length - 1] = merged;
        else
            nodes.push(node);
    };
    // Adds the given mark to the set of active marks.
    MarkdownParseState.prototype.openMark = function (mark) {
        var top = this.top();
        top.marks = mark.addToSet(top.marks);
    };
    // Removes the given mark from the set of active marks.
    MarkdownParseState.prototype.closeMark = function (mark) {
        var top = this.top();
        top.marks = mark.removeFromSet(top.marks);
    };
    MarkdownParseState.prototype.parseTokens = function (toks) {
        for (var i = 0; i < toks.length; i++) {
            var tok = toks[i];
            var handler = this.tokenHandlers[tok.type];
            if (!handler)
                throw new Error("Token type `" + tok.type + "` not supported by Markdown parser");
            handler(this, tok, toks, i);
        }
    };
    // Add a node at the current position.
    MarkdownParseState.prototype.addNode = function (type, attrs, content) {
        var top = this.top();
        var node = type.createAndFill(attrs, content, top ? top.marks : []);
        if (!node)
            return null;
        this.push(node);
        return node;
    };
    // Wrap subsequent content in a node of the given type.
    MarkdownParseState.prototype.openNode = function (type, attrs) {
        this.stack.push({ type: type, attrs: attrs, content: [], marks: prosemirror_model_1.Mark.none });
    };
    // Close and return the node that is currently on top of the stack.
    MarkdownParseState.prototype.closeNode = function () {
        var info = this.stack.pop();
        return this.addNode(info.type, info.attrs, info.content);
    };
    return MarkdownParseState;
}());
function attrs(spec, token, tokens, i) {
    if (spec.getAttrs)
        return spec.getAttrs(token, tokens, i);
    // For backwards compatibility when `attrs` is a Function
    else if (spec.attrs instanceof Function)
        return spec.attrs(token);
    else
        return spec.attrs;
}
// Code content is represented as a single token with a `content`
// property in Markdown-it.
function noCloseToken(spec, type) {
    return spec.noCloseToken || type == "code_inline" || type == "code_block" || type == "fence";
}
function withoutTrailingNewline(str) {
    return str[str.length - 1] == "\n" ? str.slice(0, str.length - 1) : str;
}
function noOp() { }
function tokenHandlers(schema, tokens) {
    var handlers = Object.create(null);
    var _loop_1 = function (type) {
        var spec = tokens[type];
        if (spec.block) {
            var nodeType_1 = schema.nodeType(spec.block);
            if (noCloseToken(spec, type)) {
                handlers[type] = function (state, tok, tokens, i) {
                    state.openNode(nodeType_1, attrs(spec, tok, tokens, i));
                    state.addText(withoutTrailingNewline(tok.content));
                    state.closeNode();
                };
            }
            else {
                handlers[type + "_open"] = function (state, tok, tokens, i) { return state.openNode(nodeType_1, attrs(spec, tok, tokens, i)); };
                handlers[type + "_close"] = function (state) { return state.closeNode(); };
            }
        }
        else if (spec.node) {
            var nodeType_2 = schema.nodeType(spec.node);
            handlers[type] = function (state, tok, tokens, i) { return state.addNode(nodeType_2, attrs(spec, tok, tokens, i)); };
        }
        else if (spec.mark) {
            var markType_1 = schema.marks[spec.mark];
            if (noCloseToken(spec, type)) {
                handlers[type] = function (state, tok, tokens, i) {
                    state.openMark(markType_1.create(attrs(spec, tok, tokens, i)));
                    state.addText(withoutTrailingNewline(tok.content));
                    state.closeMark(markType_1);
                };
            }
            else {
                handlers[type + "_open"] = function (state, tok, tokens, i) { return state.openMark(markType_1.create(attrs(spec, tok, tokens, i))); };
                handlers[type + "_close"] = function (state) { return state.closeMark(markType_1); };
            }
        }
        else if (spec.ignore) {
            if (noCloseToken(spec, type)) {
                handlers[type] = noOp;
            }
            else {
                handlers[type + "_open"] = noOp;
                handlers[type + "_close"] = noOp;
            }
        }
        else {
            throw new RangeError("Unrecognized parsing spec " + JSON.stringify(spec));
        }
    };
    for (var type in tokens) {
        _loop_1(type);
    }
    handlers.text = function (state, tok) { return state.addText(tok.content); };
    handlers.inline = function (state, tok) { return state.parseTokens(tok.children); };
    handlers.softbreak = handlers.softbreak || (function (state) { return state.addText(" "); });
    return handlers;
}
/// A configuration of a Markdown parser. Such a parser uses
/// [markdown-it](https://github.com/markdown-it/markdown-it) to
/// tokenize a file, and then runs the custom rules it is given over
/// the tokens to create a ProseMirror document tree.
var MarkdownParser = /** @class */ (function () {
    /// Create a parser with the given configuration. You can configure
    /// the markdown-it parser to parse the dialect you want, and provide
    /// a description of the ProseMirror entities those tokens map to in
    /// the `tokens` object, which maps token names to descriptions of
    /// what to do with them. Such a description is an object, and may
    /// have the following properties:
    function MarkdownParser(
    /// The parser's document schema.
    schema, 
    /// This parser's markdown-it tokenizer.
    tokenizer, 
    /// The value of the `tokens` object used to construct this
    /// parser. Can be useful to copy and modify to base other parsers
    /// on.
    tokens) {
        this.schema = schema;
        this.tokenizer = tokenizer;
        this.tokens = tokens;
        this.tokenHandlers = tokenHandlers(schema, tokens);
    }
    /// Parse a string as [CommonMark](http://commonmark.org/) markup,
    /// and create a ProseMirror document as prescribed by this parser's
    /// rules.
    ///
    /// The second argument, when given, is passed through to the
    /// [Markdown
    /// parser](https://markdown-it.github.io/markdown-it/#MarkdownIt.parse).
    MarkdownParser.prototype.parse = function (text, markdownEnv) {
        if (markdownEnv === void 0) { markdownEnv = {}; }
        var state = new MarkdownParseState(this.schema, this.tokenHandlers), doc;
        state.parseTokens(this.tokenizer.parse(text, markdownEnv));
        do {
            doc = state.closeNode();
        } while (state.stack.length);
        return doc || this.schema.topNodeType.createAndFill();
    };
    return MarkdownParser;
}());
exports.MarkdownParser = MarkdownParser;
function listIsTight(tokens, i) {
    while (++i < tokens.length)
        if (tokens[i].type != "list_item_open")
            return tokens[i].hidden;
    return false;
}
/// A parser parsing unextended [CommonMark](http://commonmark.org/),
/// without inline HTML, and producing a document in the basic schema.
exports.defaultMarkdownParser = new MarkdownParser(schema_1.schema, (0, markdown_it_1["default"])("commonmark", { html: false }), {
    blockquote: { block: "blockquote" },
    paragraph: { block: "paragraph" },
    list_item: { block: "list_item" },
    bullet_list: { block: "bullet_list", getAttrs: function (_, tokens, i) { return ({ tight: listIsTight(tokens, i) }); } },
    ordered_list: { block: "ordered_list", getAttrs: function (tok, tokens, i) { return ({
            order: +tok.attrGet("start") || 1,
            tight: listIsTight(tokens, i)
        }); } },
    heading: { block: "heading", getAttrs: function (tok) { return ({ level: +tok.tag.slice(1) }); } },
    code_block: { block: "code_block", noCloseToken: true },
    fence: { block: "code_block", getAttrs: function (tok) { return ({ params: tok.info || "" }); }, noCloseToken: true },
    hr: { node: "horizontal_rule" },
    image: { node: "image", getAttrs: function (tok) { return ({
            src: tok.attrGet("src"),
            title: tok.attrGet("title") || null,
            alt: tok.children[0] && tok.children[0].content || null
        }); } },
    hardbreak: { node: "hard_break" },
    em: { mark: "em" },
    strong: { mark: "strong" },
    link: { mark: "link", getAttrs: function (tok) { return ({
            href: tok.attrGet("href"),
            title: tok.attrGet("title") || null
        }); } },
    code_inline: { mark: "code", noCloseToken: true }
});
