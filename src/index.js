"use strict";
// Defines a parser and serializer for [CommonMark](http://commonmark.org/) text.
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
exports.__esModule = true;
exports.MarkdownSerializerState = exports.defaultMarkdownSerializer = exports.MarkdownSerializer = exports.MarkdownParser = exports.defaultMarkdownParser = exports.schema = void 0;
var schema_1 = require("./schema");
__createBinding(exports, schema_1, "schema");
var from_markdown_1 = require("./from_markdown");
__createBinding(exports, from_markdown_1, "defaultMarkdownParser");
__createBinding(exports, from_markdown_1, "MarkdownParser");
var to_markdown_1 = require("./to_markdown");
__createBinding(exports, to_markdown_1, "MarkdownSerializer");
__createBinding(exports, to_markdown_1, "defaultMarkdownSerializer");
__createBinding(exports, to_markdown_1, "MarkdownSerializerState");
