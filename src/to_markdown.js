"use strict";
exports.__esModule = true;
exports.MarkdownSerializerState = exports.defaultMarkdownSerializer = exports.MarkdownSerializer = void 0;
/// A specification for serializing a ProseMirror document as
/// Markdown/CommonMark text.
var MarkdownSerializer = /** @class */ (function () {
    /// Construct a serializer with the given configuration. The `nodes`
    /// object should map node names in a given schema to function that
    /// take a serializer state and such a node, and serialize the node.
    function MarkdownSerializer(
    /// The node serializer functions for this serializer.
    nodes, 
    /// The mark serializer info.
    marks, options) {
        if (options === void 0) { options = {}; }
        this.nodes = nodes;
        this.marks = marks;
        this.options = options;
    }
    /// Serialize the content of the given node to
    /// [CommonMark](http://commonmark.org/).
    MarkdownSerializer.prototype.serialize = function (content, options) {
        if (options === void 0) { options = {}; }
        options = Object.assign({}, this.options, options);
        var state = new MarkdownSerializerState(this.nodes, this.marks, options);
        state.renderContent(content);
        return state.out;
    };
    return MarkdownSerializer;
}());
exports.MarkdownSerializer = MarkdownSerializer;
/// A serializer for the [basic schema](#schema).
exports.defaultMarkdownSerializer = new MarkdownSerializer({
    blockquote: function (state, node) {
        state.wrapBlock("> ", null, node, function () { return state.renderContent(node); });
    },
    code_block: function (state, node) {
        // Make sure the front matter fences are longer than any dash sequence within it
        var backticks = node.textContent.match(/`{3,}/gm);
        var fence = backticks ? (backticks.sort().slice(-1)[0] + "`") : "```";
        state.write(fence + (node.attrs.params || "") + "\n");
        state.text(node.textContent, false);
        // Add a newline to the current content before adding closing marker
        state.write("\n");
        state.write(fence);
        state.closeBlock(node);
    },
    heading: function (state, node) {
        state.write(state.repeat("#", node.attrs.level) + " ");
        state.renderInline(node);
        state.closeBlock(node);
    },
    horizontal_rule: function (state, node) {
        state.write(node.attrs.markup || "---");
        state.closeBlock(node);
    },
    bullet_list: function (state, node) {
        state.renderList(node, "  ", function () { return (node.attrs.bullet || "*") + " "; });
    },
    ordered_list: function (state, node) {
        var start = node.attrs.order || 1;
        var maxW = String(start + node.childCount - 1).length;
        var space = state.repeat(" ", maxW + 2);
        state.renderList(node, space, function (i) {
            var nStr = String(start + i);
            return state.repeat(" ", maxW - nStr.length) + nStr + ". ";
        });
    },
    list_item: function (state, node) {
        state.renderContent(node);
    },
    paragraph: function (state, node) {
        state.renderInline(node);
        state.closeBlock(node);
    },
    image: function (state, node) {
        state.write("![" + state.esc(node.attrs.alt || "") + "](" + node.attrs.src.replace(/[\(\)]/g, "\\$&") +
            (node.attrs.title ? ' "' + node.attrs.title.replace(/"/g, '\\"') + '"' : "") + ")");
    },
    hard_break: function (state, node, parent, index) {
        for (var i = index + 1; i < parent.childCount; i++)
            if (parent.child(i).type != node.type) {
                state.write("\\\n");
                return;
            }
    },
    text: function (state, node) {
        state.text(node.text, !state.inAutolink);
    }
}, {
    em: { open: "*", close: "*", mixable: true, expelEnclosingWhitespace: true },
    strong: { open: "**", close: "**", mixable: true, expelEnclosingWhitespace: true },
    link: {
        open: function (state, mark, parent, index) {
            state.inAutolink = isPlainURL(mark, parent, index);
            return state.inAutolink ? "<" : "[";
        },
        close: function (state, mark, parent, index) {
            var inAutolink = state.inAutolink;
            state.inAutolink = undefined;
            return inAutolink ? ">"
                : "](" + mark.attrs.href.replace(/[\(\)"]/g, "\\$&") + (mark.attrs.title ? " \"".concat(mark.attrs.title.replace(/"/g, '\\"'), "\"") : "") + ")";
        },
        mixable: true
    },
    code: { open: function (_state, _mark, parent, index) { return backticksFor(parent.child(index), -1); }, close: function (_state, _mark, parent, index) { return backticksFor(parent.child(index - 1), 1); }, escape: false }
});
function backticksFor(node, side) {
    var ticks = /`+/g, m, len = 0;
    if (node.isText)
        while (m = ticks.exec(node.text))
            len = Math.max(len, m[0].length);
    var result = len > 0 && side > 0 ? " `" : "`";
    for (var i = 0; i < len; i++)
        result += "`";
    if (len > 0 && side < 0)
        result += " ";
    return result;
}
function isPlainURL(link, parent, index) {
    if (link.attrs.title || !/^\w+:/.test(link.attrs.href))
        return false;
    var content = parent.child(index);
    if (!content.isText || content.text != link.attrs.href || content.marks[content.marks.length - 1] != link)
        return false;
    return index == parent.childCount - 1 || !link.isInSet(parent.child(index + 1).marks);
}
/// This is an object used to track state and expose
/// methods related to markdown serialization. Instances are passed to
/// node and mark serialization methods (see `toMarkdown`).
var MarkdownSerializerState = /** @class */ (function () {
    /// @internal
    function MarkdownSerializerState(
    /// @internal
    nodes, 
    /// @internal
    marks, 
    /// The options passed to the serializer.
    options) {
        this.nodes = nodes;
        this.marks = marks;
        this.options = options;
        /// @internal
        this.delim = "";
        /// @internal
        this.out = "";
        /// @internal
        this.closed = null;
        /// @internal
        this.inAutolink = undefined;
        /// @internal
        this.atBlockStart = false;
        /// @internal
        this.inTightList = false;
        if (typeof this.options.tightLists == "undefined")
            this.options.tightLists = false;
        if (typeof this.options.hardBreakNodeName == "undefined")
            this.options.hardBreakNodeName = "hard_break";
    }
    /// @internal
    MarkdownSerializerState.prototype.flushClose = function (size) {
        if (size === void 0) { size = 2; }
        if (this.closed) {
            if (!this.atBlank())
                this.out += "\n";
            if (size > 1) {
                var delimMin = this.delim;
                var trim = /\s+$/.exec(delimMin);
                if (trim)
                    delimMin = delimMin.slice(0, delimMin.length - trim[0].length);
                for (var i = 1; i < size; i++)
                    this.out += delimMin + "\n";
            }
            this.closed = null;
        }
    };
    /// Render a block, prefixing each line with `delim`, and the first
    /// line in `firstDelim`. `node` should be the node that is closed at
    /// the end of the block, and `f` is a function that renders the
    /// content of the block.
    MarkdownSerializerState.prototype.wrapBlock = function (delim, firstDelim, node, f) {
        var old = this.delim;
        this.write(firstDelim != null ? firstDelim : delim);
        this.delim += delim;
        f();
        this.delim = old;
        this.closeBlock(node);
    };
    /// @internal
    MarkdownSerializerState.prototype.atBlank = function () {
        return /(^|\n)$/.test(this.out);
    };
    /// Ensure the current content ends with a newline.
    MarkdownSerializerState.prototype.ensureNewLine = function () {
        if (!this.atBlank())
            this.out += "\n";
    };
    /// Prepare the state for writing output (closing closed paragraphs,
    /// adding delimiters, and so on), and then optionally add content
    /// (unescaped) to the output.
    MarkdownSerializerState.prototype.write = function (content) {
        this.flushClose();
        if (this.delim && this.atBlank())
            this.out += this.delim;
        if (content)
            this.out += content;
    };
    /// Close the block for the given node.
    MarkdownSerializerState.prototype.closeBlock = function (node) {
        this.closed = node;
    };
    /// Add the given text to the document. When escape is not `false`,
    /// it will be escaped.
    MarkdownSerializerState.prototype.text = function (text, escape) {
        if (escape === void 0) { escape = true; }
        var lines = text.split("\n");
        for (var i = 0; i < lines.length; i++) {
            this.write();
            // Escape exclamation marks in front of links
            if (!escape && lines[i][0] == "[" && /(^|[^\\])\!$/.test(this.out))
                this.out = this.out.slice(0, this.out.length - 1) + "\\!";
            this.out += escape ? this.esc(lines[i], this.atBlockStart) : lines[i];
            if (i != lines.length - 1)
                this.out += "\n";
        }
    };
    /// Render the given node as a block.
    MarkdownSerializerState.prototype.render = function (node, parent, index) {
        if (typeof parent == "number")
            throw new Error("!");
        if (!this.nodes[node.type.name])
            throw new Error("Token type `" + node.type.name + "` not supported by Markdown renderer");
        this.nodes[node.type.name](this, node, parent, index);
    };
    /// Render the contents of `parent` as block nodes.
    MarkdownSerializerState.prototype.renderContent = function (parent) {
        var _this = this;
        parent.forEach(function (node, _, i) { return _this.render(node, parent, i); });
    };
    /// Render the contents of `parent` as inline content.
    MarkdownSerializerState.prototype.renderInline = function (parent) {
        var _this = this;
        this.atBlockStart = true;
        var active = [], trailing = "";
        var progress = function (node, offset, index) {
            var marks = node ? node.marks : [];
            // Remove marks from `hard_break` that are the last node inside
            // that mark to prevent parser edge cases with new lines just
            // before closing marks.
            if (node && node.type.name === _this.options.hardBreakNodeName)
                marks = marks.filter(function (m) {
                    if (index + 1 == parent.childCount)
                        return false;
                    var next = parent.child(index + 1);
                    return m.isInSet(next.marks) && (!next.isText || /\S/.test(next.text));
                });
            var leading = trailing;
            trailing = "";
            // If whitespace has to be expelled from the node, adjust
            // leading and trailing accordingly.
            if (node && node.isText && marks.some(function (mark) {
                var info = _this.marks[mark.type.name];
                return info && info.expelEnclosingWhitespace && !mark.isInSet(active);
            })) {
                var _a = /^(\s*)(.*)$/m.exec(node.text), _ = _a[0], lead = _a[1], rest = _a[2];
                if (lead) {
                    leading += lead;
                    node = rest ? node.withText(rest) : null;
                    if (!node)
                        marks = active;
                }
            }
            if (node && node.isText && marks.some(function (mark) {
                var info = _this.marks[mark.type.name];
                return info && info.expelEnclosingWhitespace &&
                    (index == parent.childCount - 1 || !mark.isInSet(parent.child(index + 1).marks));
            })) {
                var _b = /^(.*?)(\s*)$/m.exec(node.text), _ = _b[0], rest = _b[1], trail = _b[2];
                if (trail) {
                    trailing = trail;
                    node = rest ? node.withText(rest) : null;
                    if (!node)
                        marks = active;
                }
            }
            var inner = marks.length ? marks[marks.length - 1] : null;
            var noEsc = inner && _this.marks[inner.type.name].escape === false;
            var len = marks.length - (noEsc ? 1 : 0);
            // Try to reorder 'mixable' marks, such as em and strong, which
            // in Markdown may be opened and closed in different order, so
            // that order of the marks for the token matches the order in
            // active.
            outer: for (var i = 0; i < len; i++) {
                var mark = marks[i];
                if (!_this.marks[mark.type.name].mixable)
                    break;
                for (var j = 0; j < active.length; j++) {
                    var other = active[j];
                    if (!_this.marks[other.type.name].mixable)
                        break;
                    if (mark.eq(other)) {
                        if (i > j)
                            marks = marks.slice(0, j).concat(mark).concat(marks.slice(j, i)).concat(marks.slice(i + 1, len));
                        else if (j > i)
                            marks = marks.slice(0, i).concat(marks.slice(i + 1, j)).concat(mark).concat(marks.slice(j, len));
                        continue outer;
                    }
                }
            }
            // Find the prefix of the mark set that didn't change
            var keep = 0;
            while (keep < Math.min(active.length, len) && marks[keep].eq(active[keep]))
                ++keep;
            // Close the marks that need to be closed
            while (keep < active.length)
                _this.text(_this.markString(active.pop(), false, parent, index), false);
            // Output any previously expelled trailing whitespace outside the marks
            if (leading)
                _this.text(leading);
            // Open the marks that need to be opened
            if (node) {
                while (active.length < len) {
                    var add = marks[active.length];
                    active.push(add);
                    _this.text(_this.markString(add, true, parent, index), false);
                    _this.atBlockStart = false;
                }
                // Render the node. Special case code marks, since their content
                // may not be escaped.
                if (noEsc && node.isText)
                    _this.text(_this.markString(inner, true, parent, index) + node.text +
                        _this.markString(inner, false, parent, index + 1), false);
                else
                    _this.render(node, parent, index);
                _this.atBlockStart = false;
            }
            // After the first non-empty text node is rendered, the end of output
            // is no longer at block start.
            //
            // FIXME: If a non-text node writes something to the output for this
            // block, the end of output is also no longer at block start. But how
            // can we detect that?
            if ((node === null || node === void 0 ? void 0 : node.isText) && node.nodeSize > 0) {
                _this.atBlockStart = false;
            }
        };
        parent.forEach(progress);
        progress(null, 0, parent.childCount);
        this.atBlockStart = false;
    };
    /// Render a node's content as a list. `delim` should be the extra
    /// indentation added to all lines except the first in an item,
    /// `firstDelim` is a function going from an item index to a
    /// delimiter for the first line of the item.
    MarkdownSerializerState.prototype.renderList = function (node, delim, firstDelim) {
        var _this = this;
        if (this.closed && this.closed.type == node.type)
            this.flushClose(3);
        else if (this.inTightList)
            this.flushClose(1);
        var isTight = typeof node.attrs.tight != "undefined" ? node.attrs.tight : this.options.tightLists;
        var prevTight = this.inTightList;
        this.inTightList = isTight;
        node.forEach(function (child, _, i) {
            if (i && isTight)
                _this.flushClose(1);
            _this.wrapBlock(delim, firstDelim(i), node, function () { return _this.render(child, node, i); });
        });
        this.inTightList = prevTight;
    };
    /// Escape the given string so that it can safely appear in Markdown
    /// content. If `startOfLine` is true, also escape characters that
    /// have special meaning only at the start of the line.
    MarkdownSerializerState.prototype.esc = function (str, startOfLine) {
        if (startOfLine === void 0) { startOfLine = false; }
        str = str.replace(/[`*\\~\[\]_]/g, function (m, i) { return m == "_" && i > 0 && i + 1 < str.length && str[i - 1].match(/\w/) && str[i + 1].match(/\w/) ? m : "\\" + m; });
        if (startOfLine)
            str = str.replace(/^[\-*+>]/, "\\$&").replace(/^(\s*)(#{1,6})(\s|$)/, '$1\\$2$3').replace(/^(\s*\d+)\.\s/, "$1\\. ");
        if (this.options.escapeExtraCharacters)
            str = str.replace(this.options.escapeExtraCharacters, "\\$&");
        return str;
    };
    /// @internal
    MarkdownSerializerState.prototype.quote = function (str) {
        var wrap = str.indexOf('"') == -1 ? '""' : str.indexOf("'") == -1 ? "''" : "()";
        return wrap[0] + str + wrap[1];
    };
    /// Repeat the given string `n` times.
    MarkdownSerializerState.prototype.repeat = function (str, n) {
        var out = "";
        for (var i = 0; i < n; i++)
            out += str;
        return out;
    };
    /// Get the markdown string for a given opening or closing mark.
    MarkdownSerializerState.prototype.markString = function (mark, open, parent, index) {
        var info = this.marks[mark.type.name];
        var value = open ? info.open : info.close;
        return typeof value == "string" ? value : value(this, mark, parent, index);
    };
    /// Get leading and trailing whitespace from a string. Values of
    /// leading or trailing property of the return object will be undefined
    /// if there is no match.
    MarkdownSerializerState.prototype.getEnclosingWhitespace = function (text) {
        return {
            leading: (text.match(/^(\s+)/) || [undefined])[0],
            trailing: (text.match(/(\s+)$/) || [undefined])[0]
        };
    };
    return MarkdownSerializerState;
}());
exports.MarkdownSerializerState = MarkdownSerializerState;
